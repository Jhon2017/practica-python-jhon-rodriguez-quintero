from random import randrange

def sortList(list_values):
    for i in range(len(list_values)):
        index_min = i
        for k in range(i+1, len(list_values)):
            if list_values[k] < list_values[index_min]:
                index_min = k
        # Intercambio
        list_values[index_min], list_values[i] = list_values[i], list_values[index_min]
         
# Se generan la lista de 20 números
lvalues = [randrange(20) for i in range(20)]
sortList(lvalues)
print(lvalues)
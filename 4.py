buffer= [0x58A6, 0xFC89, 0xBD1A, 0x4313, 0x1250, 0x0F21, 0xC89B, 0xD1A4]
bloque = 0
for i in buffer:
    print(f'\nBloque: ', bloque+1)
    encendidos = (i & 0x8000) >> 15
    numLeds = (i & 0x7F00) >> 8
    pulso = i & 0x00FF
    str_encendidos = "Si" if encendidos else "No"
    print(f'Leds activados: {str_encendidos}\nNúmero de leds en total: {numLeds} \nPulso: {pulso}')
    bloque += 1
from itertools import permutations

def permutacion (n):
    text = ''
    for i  in range(1, n+1):
        text += str(i)
    perm = list(permutations(text, n))
    res = [''.join(tups) for tups in perm]
    result = [i+',' for i in res][:-1]
    result.append(res[-1])
    print(*result)

permutacion(4)


